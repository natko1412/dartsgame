﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dart : MonoBehaviour
{

    public float x;
    public float y;
    public float z;
    public float speed;
    public float rotation_speed;
    public bool collided;
    public bool shooting;

    public AudioClip clip;
    public AudioSource source;

    public float d;

    public DateTime shooting_time;
    
    Slider slider;

    Text points_text;
    Text hand_text;
    // Start is called before the first frame update
    void Start()
    {
        x = transform.eulerAngles.x;
        y = transform.eulerAngles.y;
        z = transform.eulerAngles.z;
        
        rotation_speed = 8f;
        collided = false;
        shooting = false;

        d = 50f*Time.deltaTime;

        slider = (Slider) GameObject.FindGameObjectWithTag ("slider").GetComponent<Slider>();
        slider.value = slider.minValue;
        speed = slider.minValue;

        points_text = (Text) GameObject.FindGameObjectWithTag ("hit_marker").GetComponent<Text>();
        hand_text = (Text) GameObject.FindGameObjectWithTag ("hand_points").GetComponent<Text>();

        

        source = (AudioSource) GameObject.FindGameObjectWithTag ("dart_source").GetComponent<AudioSource>();
        source.clip = clip;
    }
    private void OnDestroy() {
        points_text.text = "";
        hand_text.text = PlayerPrefs.GetString("hand_points");


    }

    void Update()
    {    
        
        if (Input.GetKey("space"))
        {
            if (speed >= slider.maxValue) d = -d; 
            speed += d;
            if (speed <= slider.minValue) d = -d;
            
            speed += d;

            slider.value = speed;

        }

        if (Input.GetKeyUp("space"))
        {
            
            speed = slider.value;
            shooting = true;
            shooting_time = DateTime.Now;

        }

        if (Input.GetKey("down") && !collided)
        {
            x += 0.5f;
            transform.eulerAngles = new Vector3(x, y, z);
        }

        if (Input.GetKey("up") && !collided)
        {
            x -= 0.5f;
            transform.eulerAngles = new Vector3(x, y, z);
        }

        if (Input.GetKey("left") && !collided)
        {
            y += 0.5f;
            transform.eulerAngles = new Vector3(x, y, z);
        }

        if (Input.GetKey("right") && !collided)
        {
            y -= 0.5f;
            transform.eulerAngles = new Vector3(x, y, z);
        }
        

        if (shooting && !collided) 
        {   
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            Physics.gravity = new Vector3(0, -40.0F, 0);
            transform.Translate(0, 0, -speed*Time.deltaTime);
            transform.Rotate(0, 0, rotation_speed);
            
        }
        else GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

        if (shooting && !collided && ((DateTime.Now - shooting_time).TotalSeconds) >= 3) 
        {   
            shooting = false;
            collided = true;
            GameSettings.game.new_collision(0, 1);
        }

        if (!shooting && collided && ((DateTime.Now - shooting_time).TotalSeconds) >= 1) 
        {   
            points_text.text = "";
            hand_text.text = PlayerPrefs.GetString("hand_points");
        }

        if (shooting && !collided)
        {
            RaycastHit hit;

            int layerMask = 1 << 8;
            layerMask = ~layerMask;

            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 5, layerMask))
            {   
                if (hit.collider.gameObject != null && !hit.collider.gameObject.transform.parent.gameObject.name.Contains("dart"))
                {
                    Debug.Log("VIDIM:" + hit.collider.gameObject.transform.parent.gameObject.name);
                    enter_collision(hit.collider);      
                    
                }
            
            }
        }
        



    }
    private void enter_collision(Collider other) 
    {   
        source.Play();

        

        if (other.gameObject.name.Contains("dart"))
        {
            if (!collided) {
                speed = 10f;
            }
            
            return;
        } 
        if (collided) return;
        

        speed = 0f;
        
        Debug.Log("UDARIO: " + other.gameObject.transform.parent.gameObject.name);
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        collided = true;
        shooting = false;

        string collidedFieldName;
        try 
        {
            collidedFieldName = other.gameObject.transform.parent.gameObject.name;
        }
        catch
        {
            collidedFieldName = "num0_single";
        }
        var ret = fieldNameToScoreValue(collidedFieldName);
        int collidedFieldScore = ret.Item1;
        int multiplicator = ret.Item2;
        Debug.Log("Hit: " + collidedFieldName + "; value = " + collidedFieldScore.ToString());

        if (collidedFieldScore == 0) points_text.color = Color.red;
        else points_text.color = Color.green;

        

        print(PlayerPrefs.GetInt("activeDarts"));
        if (PlayerPrefs.GetInt("activeDarts")==3)
        {
            hand_text.text = "";
            PlayerPrefs.SetString("hand_points", "");
        }
        
        points_text.text = "+" + collidedFieldScore.ToString();
        GameSettings.game.new_collision(collidedFieldScore, multiplicator);

    }
    
    private (int, int) fieldNameToScoreValue(string fieldName)
    {
        string[] splitName = fieldName.Split('_');

        if (splitName.Length < 2)
        {
            return (0, 1);
        }
        int multiplicator = 1;
        switch(splitName[1]){
            
            case "triple":
                multiplicator = 3;
                break;
            case "double":
                multiplicator = 2;
                break;
            case "single":
            default:
                multiplicator = 1;
                break;
            
            
        }
        int value = 0;
        if (splitName[0].Substring(0, 3).Equals("num"))
        {
            value = int.Parse(splitName[0].Substring(3));
        }
        else if (splitName[0].Equals("bullseye"))
        {
            value = 25;
        }

        return (value * multiplicator, multiplicator);
    }
}
