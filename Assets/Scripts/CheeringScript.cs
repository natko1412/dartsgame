﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheeringScript : MonoBehaviour
{
    public AudioClip clip;
    public AudioSource source;

    void Awake()
    {
        source.clip = clip;
        source.Play();
    }

    
}
