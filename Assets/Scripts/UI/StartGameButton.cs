﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartGameButton : MonoBehaviour
{
    public string sceneName = "";
    public Dropdown playersQuantityDropdown = null;
    public Dropdown gameTypeDropdown = null;
    public InputField[] names = null;

    public void StartGame()
    {
        GameSettings.playersQuantity = playersQuantityDropdown.value + 1;
        string gameType = gameTypeDropdown.options[gameTypeDropdown.value].text;


        //GameTypeDropdownFiller.gameTypes.TryGetValue(gameType, out GameType value);

        //GameSettings.game = new ClassicGame(int.Parse(gameType), playersQuantityDropdown.value + 1);
        GameSettings.score = gameType;
        for (int i = 0; i < 4; i++)
        {
            PlayerPrefs.SetString("name" + i.ToString(), names[i].text);
        }
        PlayerPrefs.SetInt("quantity", playersQuantityDropdown.value + 1);
        SceneManager.LoadScene(sceneName);
        Debug.Log("Started game: " + GameSettings.toString());
    }
    void Update()
    {
        for (int i = 0; i < 4; i++)
        {
            if (i > playersQuantityDropdown.value)
            {
                names[i].enabled = false;
                names[i].transform.Find("Text").GetComponent<Text>().color = Color.grey;

            } 
            else {
                names[i].enabled = true;
                names[i].transform.Find("Text").GetComponent<Text>().color = Color.black  ;

            }
        }
    }
}
