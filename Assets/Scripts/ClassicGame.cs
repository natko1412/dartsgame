﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>Class <c>ClassicGame</c> models a classic darts game</summary>
///
public class ClassicGame : MonoBehaviour
{

    public int game_type;
    public int n_players;
    public int[] scores;
    public int current_player;
    public int current_throw;
    public int new_score;
    public int hand_score;
    public bool game_finished;

    public GameObject dart;

    private UpdateScoreboard scoreBoard;
    Text points_text;
    Text hand_text;

    
    void Awake()
    {
        this.game_type = int.Parse(GameSettings.score);
        this.n_players = GameSettings.playersQuantity;
        this.current_player = 0;
        this.current_throw = 0;
        this.game_finished = false;
        this.hand_score = 0;


        this.scores = new int[n_players];

        for (int i = 0; i < n_players; ++i)
        {
            scores[i] = game_type;
        }

        this.new_score = game_type;

        scoreBoard = GetComponent<UpdateScoreboard>();

        GameSettings.game = this;
    }

    void Start()
    {

        points_text = (Text) GameObject.FindGameObjectWithTag ("hit_marker").GetComponent<Text>();
        hand_text = (Text) GameObject.FindGameObjectWithTag ("hand_points").GetComponent<Text>();

        System.Random r = new System.Random();
        int x, y;
        x = r.Next(-15, 40);
        y = r.Next(150, 200);

        Instantiate(dart, new Vector3(0, 20, -35), Quaternion.Euler(x, y, 50));
    }

    void Update()
    {
        
        PlayerPrefs.SetInt("activeDarts", 3 - current_throw);
        for (int i = 0; i < n_players; ++i)
        {
            if (i == current_player)
            {
                scoreBoard.players[i].color = Color.blue;
            }
            else
            {
                scoreBoard.players[i].color = Color.black;
            }
        }
    }

    public bool new_collision(int value, int multiplicator)
    {
        this.current_throw++;
        this.new_score -= value;
        this.hand_score += value;
        PlayerPrefs.SetString("hand_points", hand_score.ToString());
        
        updateScoreboard(this.current_player, this.new_score);

        if (this.new_score == 0)
        {
            if (PlayerPrefs.GetInt("double_out") == 0 || multiplicator == 2) game_over(current_player);
            else next_player(false);

        }

        if (this.new_score == 1 && PlayerPrefs.GetInt("double_out") == 1)
        {
            next_player(false);

        }
        if (this.new_score < 0)
        {

            next_player(false);
            

        }

        
        if (this.current_throw == 3)
        {
            this.current_throw = 0;
            next_player(true);
            

        }
       
        System.Random r = new System.Random();
        int x, y;
        x = r.Next(-15, 40);
        y = r.Next(150, 200);
        Instantiate(dart, new Vector3(0, 20, -35), Quaternion.Euler(x, y, 50));

        return false;
    }


    /// <summary>method <c>next_player</c> switches to next player.</summary>
    private void next_player(bool regularScore)
    {

        if (regularScore)
        {
            this.scores[this.current_player] = this.new_score;
        }
        else {

            //ak je negativan score, ispravi score board
            int correct_score = this.scores[this.current_player];
            //update scoreboard
            updateScoreboard(this.current_player, correct_score);

        }
        

        this.current_throw = 0;
        this.hand_score = 0;
        this.current_player = (this.current_player + 1) % this.n_players;
        this.new_score = this.scores[this.current_player];

        points_text.text = "";
        hand_text.text = "";
        DateTime t = DateTime.Now;

        
        
        
        delete_darts();
        
    }

    /// <summary>method <c>delete_darts</c> destroys all dart objects from the scene.</summary>
    private void delete_darts()
    {   

        System.Threading.Thread.Sleep(500);

        GameObject[] darts;
 
        darts = GameObject.FindGameObjectsWithTag("dart");
        
        foreach(GameObject dart in darts)
        {
            Destroy(dart);
        }
        PlayerPrefs.SetInt("activeDarts", 3);
    }

    /// <summary>method <c>game_over</c> switches scene to WinScreen scene.</summary>
    private void game_over(int current_player)
    {   

        PlayerPrefs.SetInt("winner", current_player);
        SceneManager.LoadScene("WinScreen");                 
        
    }

    /// <summary>method <c>draw</c> updates scores on the scoreboard.</summary>
    void updateScoreboard(int player, int score)
    {

        Debug.Log("player: " + player.ToString() + "; score: " + score.ToString());
        scoreBoard.update(player, score);
    }
}